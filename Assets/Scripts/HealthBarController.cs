﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthBarController : MonoBehaviour
{
  public ProgressBar ProgressBarPrefab;

  private readonly Dictionary<Health, ProgressBar> _healthBars = new Dictionary<Health, ProgressBar>();

  private void Awake()
  {
    Health.OnHealthAdded += HandleHealthAdded;
    Health.OnHealthRemoved += HandleHealthRemoved;
  }

  private void OnDestroy()
  {
    Health.OnHealthAdded -= HandleHealthAdded;
    Health.OnHealthRemoved -= HandleHealthRemoved;
  }

  private void HandleHealthAdded(Health health)
  {
    if (_healthBars.ContainsKey(health))
      return;

    ProgressBar progressBar = Instantiate(ProgressBarPrefab, transform);
    _healthBars.Add(health, progressBar);
    progressBar.Value = health;
  }

  private void HandleHealthRemoved(Health health)
  {
    if (_healthBars.ContainsKey(health))
    {
      Destroy(_healthBars[health].gameObject);
      _healthBars.Remove(health);
    }
  }
}
