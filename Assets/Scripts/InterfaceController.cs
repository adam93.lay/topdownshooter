﻿using TMPro;
using UnityEngine;

public class InterfaceController : MonoBehaviour
{
  public ProgressBar PlayerXpBar;
  public TextMeshProUGUI LevelText;

  private void Start()
  {
    var player = FindObjectOfType<Player>();
    var playerXp = player.GetComponent<Experience>();

    PlayerXpBar.Value = playerXp;
    playerXp.OnLevelChanged += HandleLevelChanged;
    HandleLevelChanged(playerXp);
  }

  private void HandleLevelChanged(Experience xp)
  {
    LevelText.text = xp.Level.ToString();
  }
}
