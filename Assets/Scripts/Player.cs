﻿using UnityEngine;

public class Player : MonoBehaviour
{
  public float Speed = 10f;

  private float _moveX;
  private float _moveY;
  private Rigidbody2D _rb;

  private void Awake()
  {
    _rb = GetComponent<Rigidbody2D>();

    GetComponent<Health>().OnDied += HandleDied;
  }

  private void HandleDied(Health hp)
  {
    Destroy(gameObject);
  }

  private void FixedUpdate()
  {
    _moveX = Input.GetKey(KeyCode.A) ? -1 : Input.GetKey(KeyCode.D) ? 1 : 0;
    _moveY = Input.GetKey(KeyCode.S) ? -1 : Input.GetKey(KeyCode.W) ? 1 : 0;

    _rb.velocity = new Vector2(_moveX, _moveY).normalized * Speed;
  }

  private void Update()
  {
    // Look at mouse
    transform.TopDownLookAt(Camera.main.ScreenToWorldPoint(Input.mousePosition));
  }
}
