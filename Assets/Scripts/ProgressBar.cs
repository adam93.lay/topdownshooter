﻿using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ProgressBar : MonoBehaviour
{
  private TrackableValue _value;
  public TrackableValue Value
  {
    get => _value;
    set
    {
      _value = value;
      _value.OnChanged += HandleValueChanged;
      HandleValueChanged(_value);
    }
  }

  public float PositionOffset;
  public bool DisplayText;
  public bool DisplayTextPercentage;
  public string DisplayTextSuffix;

  private Image _foregroundImage;
  private Coroutine _inProgressCoroutine;
  private float _lastTargetPrc;
  private TextMeshProUGUI _text;

  private void Awake()
  {
    _foregroundImage = transform.Find("Foreground").GetComponent<Image>();
    _text = transform.Find("Text").GetComponent<TextMeshProUGUI>();
  }

  private void OnDisable()
  {
    Value.OnChanged -= HandleValueChanged;
  }

  private void LateUpdate()
  {
    if (Value is Health h)
    {
      transform.position = Camera.main.WorldToScreenPoint(h.transform.position + Vector3.up * PositionOffset);
    }
  }

  private void HandleValueChanged(TrackableValue value)
  {
    if (_inProgressCoroutine != null)
    {
      StopCoroutine(_inProgressCoroutine);
      _foregroundImage.fillAmount = _lastTargetPrc;
    }

    _lastTargetPrc = value.CurrentValue / value.MaxValue;
    _inProgressCoroutine = StartCoroutine(UpdateBar(_lastTargetPrc));

    _text.text = DisplayText ? $"{value.CurrentValue:N1}/{value.MaxValue:N1}{DisplayTextSuffix} {(DisplayTextPercentage ? $"({_lastTargetPrc * 100:N2}%)" : string.Empty)}" : string.Empty;
  }

  private IEnumerator UpdateBar(float prc)
  {
    float original = _foregroundImage.fillAmount;
    float updateSpeed = 0.25f;
    float elapsed = 0f;

    while (elapsed < updateSpeed)
    {
      elapsed += Time.deltaTime;
      _foregroundImage.fillAmount = Mathf.SmoothStep(original, prc, elapsed / updateSpeed);
      yield return null;
    }

    _foregroundImage.fillAmount = prc;
    _inProgressCoroutine = null;
  }
}
