﻿using System;
using UnityEngine;

public class ChaseState : BaseState
{
  private readonly Enemy _enemy;
  private readonly Rigidbody2D _rb;

  public ChaseState(Enemy enemy) : base(enemy.gameObject)
  {
    _enemy = enemy;
    _rb = enemy.GetComponent<Rigidbody2D>();
  }

  public override Type Update()
  {
    // If no target
    if (_enemy.Target == null)
      return typeof(WanderState);

    // If lost sight of target
    if (_enemy.DistanceToTarget > _enemy.LoseSightRadius)
    {
      _enemy.SetTarget(null);
      return typeof(WanderState);
    }

    // Look at target
    transform.TopDownLookAt(_enemy.Target.position);

    Vector2 direction = (_enemy.Target.position - transform.position).normalized;
    _rb.MovePosition((Vector2)transform.position + direction * _enemy.Speed * Time.deltaTime);

    return _enemy.DistanceToTarget <= _enemy.AttackRadius ? typeof(AttackState) : null;
  }
}
