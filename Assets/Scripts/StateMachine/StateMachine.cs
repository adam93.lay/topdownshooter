﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class StateMachine : MonoBehaviour
{
  public BaseState CurrentState { get; private set; }

  public event Action<BaseState> OnStateChanged;

  private Dictionary<Type, BaseState> _states;

  public void SetStates(Dictionary<Type, BaseState> states)
  {
    _states = states;
  }

  private void Update()
  {
    if (CurrentState == null)
    {
      CurrentState = _states.First().Value;
    }

    Type newState = CurrentState.Update();

    if (newState != null && newState != CurrentState.GetType())
    {
      SwitchState(newState);
    }
  }

  private void SwitchState(Type newState)
  {
    CurrentState = _states[newState];
    OnStateChanged?.Invoke(CurrentState);
  }
}
