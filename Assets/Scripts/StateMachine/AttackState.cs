﻿using System;
using UnityEngine;

public class AttackState : BaseState
{
  private readonly Enemy _enemy;
  private float _attackTime;

  public AttackState(Enemy enemy) : base(enemy.gameObject)
  {
    _enemy = enemy;
  }

  public override Type Update()
  {
    // If we don't have a target any more
    if (_enemy.Target == null)
      return typeof(WanderState);

    // If we're out of attack range
    if (_enemy.DistanceToTarget > _enemy.AttackRadius)
      return typeof(ChaseState);

    // Look at enemy
    transform.TopDownLookAt(_enemy.Target.position);

    // Attack!
    _attackTime += Time.deltaTime;

    if (_attackTime >= _enemy.AttackRate)
    {
      var targetHealth = _enemy.Target.GetComponent<Health>();
      targetHealth.Modify(-_enemy.AttackDamage);
      _attackTime = 0;
    }

    return null;
  }
}
