﻿using UnityEngine;

public class WanderState : BaseState
{
  private readonly Enemy _enemy;
  public float RandomAmplitude = 2;

  private Vector2 _dest;
  private readonly Rigidbody2D _rb;
  #region Rotation
  private float _turnElapsed;
  private float _turnTime = 0.25f;
  private Quaternion _originalRotation;
  private Quaternion _targetRotation;
  #endregion

  public WanderState(Enemy enemy) : base(enemy.gameObject)
  {
    _enemy = enemy;
    _rb = enemy.GetComponent<Rigidbody2D>();
    GetNewDestination();
  }

  public override System.Type Update()
  {
    // Check for target
    Transform target = CanSeeTarget();
    if (target != null)
    {
      _enemy.SetTarget(target);
      return typeof(ChaseState);
    }

    if (((Vector2)transform.position - _dest).magnitude < 0.25f)
    {
      GetNewDestination();
    }

    transform.rotation = Quaternion.Slerp(_originalRotation, _targetRotation, Mathf.Clamp01((_turnElapsed += Time.deltaTime) / _turnTime));

    Vector2 direction = (_dest - (Vector2)transform.position).normalized;
    _rb.MovePosition((Vector2)transform.position + direction * _enemy.Speed * Time.deltaTime);

    Debug.DrawLine(transform.position, _dest);

    return null;
  }

  private void GetNewDestination()
  {
    // Set destination
    _dest = new Vector2(
      transform.position.x + Random.Range(-RandomAmplitude, RandomAmplitude),
      transform.position.y + Random.Range(-RandomAmplitude, RandomAmplitude));

    Vector2 directionToFace = _dest - (Vector2)transform.position;
    float angle = Mathf.Atan2(directionToFace.y, directionToFace.x) * Mathf.Rad2Deg;
    _targetRotation = Quaternion.AngleAxis(angle, Vector3.forward);
    _originalRotation = transform.rotation;
    _turnElapsed = 0;
  }

  private Transform CanSeeTarget()
  {
    RaycastHit2D hit = Physics2D.CircleCast(transform.position, _enemy.SightRadius, Vector2.zero, 0, LayerMask.GetMask("Player"));
    return hit.transform;
  }
}
