﻿using System;
using UnityEngine;

public abstract class BaseState
{
  protected readonly GameObject gameObject;
  protected Transform transform;

  protected BaseState(GameObject gameObject)
  {
    this.gameObject = gameObject;
    this.transform = gameObject.transform;
  }

  /// <returns>Type of state to change to</returns>
  public abstract Type Update();
}
