﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
  public float Speed;
  public float SightRadius;
  public float LoseSightRadius;
  public float AttackRadius;
  public float AttackRate;
  public float AttackDamage;

  public float DistanceToTarget => Vector3.Distance(transform.position, Target.position);

  private StateMachine _stateMachine;
  public Transform Target { get; private set; }

  private void Start()
  {
    _stateMachine = GetComponent<StateMachine>();

    _stateMachine.SetStates(new Dictionary<Type, BaseState>
    {
      { typeof(WanderState), new WanderState(this) },
      { typeof(ChaseState), new ChaseState(this) },
      { typeof(AttackState), new AttackState(this) },
    });

    var health = this.GetComponent<Health>();

    health.OnDied += _ =>
    {
      gameObject.SetActive(false);
      //Destroy(gameObject);
    };
  }

  public void SetTarget(Transform target)
  {
    Target = target;
  }

  private void OnDrawGizmos()
  {
    Gizmos.color = new Color(1, 0, 0, 0.2f);
    Gizmos.DrawSphere(transform.position, SightRadius);
  }
}
