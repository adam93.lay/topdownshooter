﻿using UnityEngine;

[RequireComponent(typeof(Health))]
public class XpOnDeath : MonoBehaviour
{
  public float XpValue;
}
