﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
  public static GameController Instance;
  public Player Player { get; private set; }

  public GameObject PauseScreen;

  private void Awake()
  {
    Instance = this;

    Player = FindObjectOfType<Player>();
  }

  private void Update()
  {
    if (Input.GetKeyDown(KeyCode.Escape))
    {
      Pause();
    }
  }

  public void Pause()
  {
    Time.timeScale = 0;
    PauseScreen.SetActive(true);
  }

  public void Resume()
  {
    Time.timeScale = 1;
    PauseScreen.SetActive(false);
  }

  public void BackToHub()
  {
    SceneManager.LoadScene("SceneBase", LoadSceneMode.Single);
    SceneManager.LoadScene("MissionHub", LoadSceneMode.Additive);

    Resume();
  }

  public void LoadMission()
  {
    SceneManager
      .UnloadSceneAsync("MissionHub")
      .completed += _ => SceneManager.LoadScene("TestLevelScene", LoadSceneMode.Additive);

    Player.transform.position = Vector3.zero;

    Resume();
  }
}
