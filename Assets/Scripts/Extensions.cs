﻿using UnityEngine;

public static class Extensions
{
  public static void TopDownLookAt(this Transform self, Vector2 target)
  {
    Vector2 dir = target - (Vector2)self.position;
    float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
    self.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
  }
}
