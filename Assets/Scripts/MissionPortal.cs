﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissionPortal : MonoBehaviour
{
  private void OnTriggerEnter2D(Collider2D col)
  {
    var player = col.GetComponent<Player>();
    if (player == null)
      return;

    GameController.Instance.LoadMission();
  }
}
