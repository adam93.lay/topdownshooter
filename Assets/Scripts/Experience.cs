﻿using System;
using UnityEngine;

public class Experience : TrackableValue
{
  [SerializeField]
  private int _level = 1;
  [SerializeField]
  private float _baseLevelCost = 50.0f;
  [SerializeField]
  private float _multiplier = 1.07f;

  public int Level => _level;
  public float NextLevelCost => Mathf.Round(_baseLevelCost * Mathf.Pow(_multiplier, _level));

  public event Action<Experience> OnLevelChanged;

  private void OnEnable()
  {
    MaxValue = NextLevelCost;
  }

  public override void Modify(float value)
  {
    _currentValue += value;

    // Level up!
    if (_currentValue >= NextLevelCost)
    {
      // Carry over remainder of XP
      _currentValue = Mathf.Max(_currentValue - NextLevelCost, 0);

      // Increment Level
      _level++;

      MaxValue = NextLevelCost;

      OnLevelChanged?.Invoke(this);
    }

    NotifyChanged();
  }
}
