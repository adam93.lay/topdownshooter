﻿using UnityEngine;

[RequireComponent(typeof(ParticleSystem))]
public class DestroyAfterPlay : MonoBehaviour
{
  private float _timeLeft;

  private void Awake()
  {
    _timeLeft = GetComponent<ParticleSystem>().main.duration;
  }

  private void Update()
  {
    _timeLeft -= Time.deltaTime;
    if (_timeLeft <= 0.0f)
      Destroy(gameObject);
  }
}
