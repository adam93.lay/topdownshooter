﻿using System;
using UnityEngine;

public class Health : TrackableValue
{
  public bool IsDead => CurrentValue <= 0;
  public event Action<Health> OnDied;

  public static event Action<Health> OnHealthAdded;
  public static event Action<Health> OnHealthRemoved;

  private void OnEnable()
  {
    Modify(MaxValue);
    OnHealthAdded?.Invoke(this);
  }

  private void OnDisable()
  {
    OnHealthRemoved?.Invoke(this);
  }

  public override void Modify(float value)
  {
    base.Modify(value);

    if (IsDead)
      OnDied?.Invoke(this);
  }
}
