﻿using UnityEngine;

[RequireComponent(typeof(Weapon))]
public class ProjectileWeapon : MonoBehaviour
{
  public Transform Barrel;
  public GameObject BulletPrefab;

  private Weapon _weapon;

  private void Awake()
  {
    _weapon = GetComponent<Weapon>();
  }

  private void Update()
  {
    if (!_weapon.Fire())
      return;

    // Create a Bullet at the position of the Barrel
    GameObject bullet = Instantiate(BulletPrefab, Barrel.position, transform.rotation);

    // Set the Bullet's source to this
    bullet.GetComponent<Bullet>().OnHit += _weapon.HandleOnHit;
  }
}
