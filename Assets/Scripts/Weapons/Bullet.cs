﻿using System;
using UnityEngine;

public class Bullet : MonoBehaviour
{
  public GameObject Source;
  public float Speed;
  public float Damage;

  public event Action<Transform, Vector2> OnHit;

  private Rigidbody2D _rb;
  private float _lifetime = 10;

  private void Awake()
  {
    _rb = GetComponent<Rigidbody2D>();
  }

  private void FixedUpdate()
  {
    _rb.velocity = transform.right.normalized * Speed;
  }

  private void Update()
  {
    _lifetime -= Time.deltaTime;
    if (_lifetime <= 0)
      Destroy(gameObject);
  }

  private void OnCollisionEnter2D(Collision2D col)
  {
    // Do not collider with self
    if (col.gameObject.name == "Player")
      return;

    OnHit?.Invoke(col.transform, col.contacts[0].point);

    // Destroy effect

    Destroy(gameObject);
  }
}
