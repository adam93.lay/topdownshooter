﻿using UnityEngine;

public class Weapon : MonoBehaviour
{
  public float FireRate = 1f;
  public int MaxAmmo = 10;
  public float Damage = 2f;
  public ParticleSystem HitEffect;

  public int CurrentAmmo { get; private set; }

  public bool CanFire => _nextShotTime <= 0 && CurrentAmmo > 0;

  private int _maxAmmo;
  private float _nextShotTime;

  private void OnEnable()
  {
    CurrentAmmo = MaxAmmo;
  }

  private void Update()
  {
    _nextShotTime = Mathf.Clamp(_nextShotTime - Time.deltaTime, 0, FireRate);
  }

  public bool Fire()
  {
    if (!Input.GetMouseButton(0))
      return false;

    if (!CanFire)
      return false;

    CurrentAmmo--;
    _nextShotTime = FireRate;

    return true;
  }

  public void HandleOnHit(Transform target, Vector2 impactPoint)
  {
    // Play hit effect
    Instantiate(HitEffect, impactPoint, Quaternion.identity);

    var health = target.GetComponent<Health>();
    if (health == null)
      return;

    // Handle damage falloff

    // Handle AOE damage

    // Inflict damage
    health.Modify(-Damage);

    // If object was killed
    if (!health.IsDead)
      return;

    // Notify source if XP on death
    var weaponHolderExperience = GetComponent<Experience>();
    var reward = target.GetComponent<XpOnDeath>();
    if (weaponHolderExperience != null && reward != null)
    {
      weaponHolderExperience.Modify(reward.XpValue);
    }
  }
}
