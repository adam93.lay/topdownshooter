﻿using UnityEngine;

public class HitscanWeapon : MonoBehaviour
{
  public float Range;

  private Weapon _weapon;

  private void Awake()
  {
    _weapon = GetComponent<Weapon>();
  }

  private void Update()
  {
    Debug.DrawLine(transform.position, transform.position + transform.right * Range);

    // Try and fire
    if (!_weapon.Fire())
      return;

    // Raycast
    RaycastHit2D hit = Physics2D.Raycast(
      transform.position, 
      transform.right, 
      Range, 
      ~LayerMask.GetMask("Player"));

    if (hit.transform == null)
      return;

    _weapon.HandleOnHit(hit.transform, hit.point);
  }
}
