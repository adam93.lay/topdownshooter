﻿using UnityEngine;

public class Spawner : MonoBehaviour
{
  public GameObject Prefab;
  public float Frequency;
  public int Max;

  private float _spawnTime;
  private int _spawnedSoFar;

  public void Update()
  {
    _spawnTime += Time.deltaTime;

    // If it's time to spawn!
    if (_spawnTime >= Frequency)
    {
      // Reset spawn time
      _spawnTime = 0;
      _spawnedSoFar++;

      Instantiate(Prefab, transform.position, transform.rotation);
    }

    if (_spawnedSoFar >= Max)
      gameObject.SetActive(false);
  }
}
