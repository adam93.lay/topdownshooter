﻿using System;
using UnityEngine;

public class TrackableValue : MonoBehaviour
{
  public float MaxValue;
  [SerializeField]
  protected float _currentValue;

  public float CurrentValue => _currentValue;

  public event Action<TrackableValue> OnChanged;

  public virtual void Modify(float value)
  {
    _currentValue += value;

    NotifyChanged();
  }

  protected void NotifyChanged()
  {
    OnChanged?.Invoke(this);
  }
}
