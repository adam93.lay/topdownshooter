﻿using UnityEngine;

public class RotateAroundTarget : MonoBehaviour
{
  public Transform Target;
  public float Speed;
  public float Radius;
  public float Amplitude;
  public float Frequency;
  public float Noise = 0.1f;

  private float _t;

  private float GetNoise() => Random.Range(-Noise, Noise);

  private void Update()
  {
    _t += Time.deltaTime * Speed;
    
    float x = (Radius + Amplitude * Mathf.Sin(Frequency * _t)) * Mathf.Cos(_t);
    float y = (Radius + Amplitude * Mathf.Sin(Frequency * _t)) * Mathf.Sin(_t);
    
    var pos = new Vector3(x, y, 0);

    transform.position = Target.position + pos;
  }
}
